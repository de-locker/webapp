import { metaObject } from "@/config/site.config";

import ListRoom from "../shared/guest/list-room";

export const metadata = {
  ...metaObject("All Rooms"),
};

export default function GuestPage() {
  return (
    <>
      <ListRoom />
    </>
  );
}
