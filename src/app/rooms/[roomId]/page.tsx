import RoomDetail from "@/app/shared/room/room-detail";
import { metaObject } from "@/config/site.config";

export const metadata = {
  ...metaObject("Room Detail"),
};

export default function RoomDetailPage() {
  return <RoomDetail />;
}
