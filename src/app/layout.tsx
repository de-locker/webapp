import type { Metadata } from "next";
import "@/app/globals.css";

import { headers } from "next/headers";
import { Toaster } from "react-hot-toast";
import { cookieToInitialState } from "wagmi";

import { inter, lexendDeca } from "@/app/fonts";
import GlobalDrawer from "@/app/shared/drawer-views/container";
import GlobalModal from "@/app/shared/modal-views/container";
import { config } from "@/config/web3";
import HydrogenLayout from "@/layouts/hydrogen/layout";
import { cn } from "@/utils/class-names";

import Providers from "./providers";

export const metadata: Metadata = {
  title: "App Name",
  description: "Write your app description",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const initialState = cookieToInitialState(config, headers().get("cookie"));

  return (
    <html
      // 💡 Prevent next-themes hydration warning
      suppressHydrationWarning
    >
      <body
        // 💡 Prevent hydration warnings caused by third-party extensions, such as Grammarly.
        suppressHydrationWarning
        className={cn(inter.variable, lexendDeca.variable, "font-inter")}
      >
        <Providers initialWagmiState={initialState}>
          <HydrogenLayout>{children}</HydrogenLayout>
          <Toaster />
          <GlobalDrawer />
          <GlobalModal />
        </Providers>
      </body>
    </html>
  );
}
