"use client";

import Link from "next/link";
import { ReactNode } from "react";
import { Address } from "viem";

import useChain from "@/hooks/use-chain";
import { truncateEthAddress } from "@/utils/web3";

export default function Web3Address({
  chainId,
  address,
  truncate,
  children,
}: {
  chainId: string;
  address: Address;
  truncate?: boolean;
  children?: ReactNode;
}) {
  const chain = useChain(Number(chainId));

  const displayText = children
    ? children
    : truncate
    ? truncateEthAddress(address)
    : address;

  if (chain) {
    return (
      <Link
        className="text-primary hover:text-primary-dark"
        href={`${chain?.blockExplorers?.default.url}/address/${address}`}
        target="_blank"
        rel="nofollow noopener noreferrer"
      >
        {displayText}
      </Link>
    );
  }

  return displayText;
}
