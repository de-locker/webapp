"use client";

import Link from "next/link";
import { ReactNode } from "react";

import useChain from "@/hooks/use-chain";

export default function Web3Transaction({
  chainId,
  hash,
  children,
}: {
  chainId: string;
  hash: string;
  children?: ReactNode;
}) {
  const chain = useChain(Number(chainId));

  const displayText = children ? children : hash;

  if (chain) {
    return (
      <Link
        className="text-primary hover:text-primary-dark"
        href={`${chain?.blockExplorers?.default.url}/tx/${hash}`}
        target="_blank"
        rel="nofollow noopener noreferrer"
      >
        {displayText}
      </Link>
    );
  }

  return displayText;
}
