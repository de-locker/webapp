import { ReactNode } from "react";
import { Title } from "rizzui";

import cn from "@/utils/class-names";

export default function SectionBlock({
  title,
  titleClassName,
  action,
  children,
  className,
}: React.PropsWithChildren<{
  title?: string;
  titleClassName?: string;
  action?: ReactNode;
  className?: string;
}>) {
  return (
    <section className={className}>
      <header className="mb-2.5 lg:mb-3 flex gap-4 items-center">
        <Title
          as="h5"
          className={cn(
            "mb-2 text-sm font-normal text-gray-700 sm:text-base flex",
            titleClassName
          )}
        >
          {title}
        </Title>
        {action}
      </header>

      {children}
    </section>
  );
}
