"use client";

import { useEffect, useState } from "react";
import { Address } from "viem";
import { useAccount, useChainId } from "wagmi";

import useLockerFactory from "@/hooks/use-locker-factory";

import RoomCard from "../room/room-card";
import SectionBlock from "../section-block";
import CreateRoom from "./create-room";

export default function ListRoom() {
  const chainId = useChainId();
  const account = useAccount();
  const [lockers, setLockers] = useState<readonly Address[]>([]);

  const { lockersOf } = useLockerFactory(chainId.toString());

  useEffect(() => {
    if (!account.address) return;
    lockersOf(account.address).then(setLockers);
  }, [account.address, lockersOf]);

  return (
    <div className="px-4">
      <SectionBlock
        title="Your Rooms"
        titleClassName="text-xl sm:text-2xl"
        action={<CreateRoom />}
      >
        <div className="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 3xl:gap-8">
          {lockers.map((locker) => (
            <RoomCard key={locker} address={locker} />
          ))}
        </div>
      </SectionBlock>
    </div>
  );
}
