"use client";

import { useCallback, useEffect, useMemo, useState } from "react";
import { SubmitHandler } from "react-hook-form";
import toast from "react-hot-toast";
import { PiPlus, PiXLight } from "react-icons/pi";
import { ActionIcon, Button, Input, Modal, Title } from "rizzui";
import { useChainId } from "wagmi";
import { z } from "zod";

import { Form } from "@/components/ui/form";
import useLockerFactory from "@/hooks/use-locker-factory";

import Web3Transaction from "../web3/web3-transaction";

export default function CreateRoom() {
  const [modalState, setModalState] = useState(false);
  const [reset, setReset] = useState({});
  const chainId = useChainId();

  const { newLocker } = useLockerFactory(chainId.toString());

  const schema = useMemo(
    () =>
      z.object({
        name: z.string().min(1),
        symbol: z.string().min(1),
      }),
    []
  );
  type Schema = z.infer<typeof schema>;

  const initialValues = useMemo(
    () => ({
      name: "",
      symbol: "",
    }),
    []
  );

  const onSubmit: SubmitHandler<Schema> = useCallback(
    (data) => {
      const { name, symbol } = data;

      newLocker({
        name,
        symbol,
        onSuccess: (hash) => {
          toast.success(() => (
            <div className="max-w-72 truncate">
              <span>Created.&nbsp;</span>
              <Web3Transaction chainId={chainId.toString()} hash={hash} />
            </div>
          ));
          setReset(initialValues);
          setModalState(false);
        },
      });
    },
    [chainId, initialValues, newLocker]
  );

  useEffect(() => {
    setReset(initialValues);
  }, [initialValues]);

  return (
    <>
      <ActionIcon
        onClick={() => setModalState(true)}
        variant="outline"
        rounded="full"
      >
        <PiPlus />
      </ActionIcon>
      <Modal isOpen={modalState} onClose={() => setModalState(false)}>
        <div className="m-auto px-7 pb-8 pt-6">
          <div className="mb-7 flex items-center justify-between">
            <Title as="h3">Create new room</Title>
            <ActionIcon
              size="sm"
              variant="text"
              onClick={() => setModalState(false)}
            >
              <PiXLight />
            </ActionIcon>
          </div>
          <Form<Schema>
            className="gap-x-5 gap-y-3 [&_label>span]:font-medium"
            validationSchema={schema}
            resetValues={reset}
            onSubmit={onSubmit}
            useFormProps={{
              mode: "onChange",
              defaultValues: initialValues,
            }}
          >
            {({ register, formState: { errors } }) => (
              <div className="grid grid-cols-1 gap-y-6">
                <Input
                  className="col-span-8"
                  type="text"
                  label="Name"
                  placeholder="A25 Hotel - Room 696"
                  {...register("name")}
                  error={errors.name?.message}
                />
                <Input
                  className="col-span-8"
                  type="text"
                  label="Shortname"
                  placeholder="A25-R696"
                  {...register("symbol")}
                  error={errors.name?.message}
                />
                <Button type="submit" className="col-span-12">
                  Create
                </Button>
              </div>
            )}
          </Form>
        </div>
      </Modal>
    </>
  );
}
