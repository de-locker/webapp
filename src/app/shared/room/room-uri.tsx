"use client";

import Link from "next/link";
import { useEffect, useState } from "react";
import { Button } from "rizzui";
import { Address, numberToHex } from "viem";

import useGreenfield from "@/hooks/use-greenfield";
import useLocker from "@/hooks/use-locker";

export default function RoomURI({
  chainId,
  lockerAddress,
  tokenId,
}: {
  chainId: string;
  lockerAddress: Address;
  tokenId: number;
}) {
  const { tokenURI } = useLocker(chainId, lockerAddress);
  const { getObject } = useGreenfield();
  const [uri, setURI] = useState<string>();
  const [objectId, setObjectId] = useState<string>();

  useEffect(() => {
    tokenURI({ tokenId: BigInt(tokenId) }).then(setURI);
  }, [tokenId, tokenURI]);

  useEffect(() => {
    if (!uri) return;

    getObject({ objectName: uri }).then((resp) =>
      setObjectId(numberToHex(Number(resp.objectInfo?.id) || 0, { size: 32 }))
    );
  }, [getObject, uri]);

  return (
    objectId && (
      <Link
        href={`https://testnet.greenfieldscan.com/object/${objectId}`}
        target="_blank"
      >
        <Button variant="text">View</Button>
      </Link>
    )
  );
}
