"use client";

import dayjs from "dayjs";
import { useCallback, useEffect, useMemo, useState } from "react";
import { Controller, SubmitHandler } from "react-hook-form";
import toast from "react-hot-toast";
import { PiPlus, PiXLight } from "react-icons/pi";
import { ActionIcon, Button, Input, Modal, Title } from "rizzui";
import { Address } from "viem";
import { z } from "zod";

import DatePicker from "@/components/datepicker";
import { Form } from "@/components/ui/form";
import useGreenfield from "@/hooks/use-greenfield";
import useLocker from "@/hooks/use-locker";

import Web3Transaction from "../web3/web3-transaction";

export default function AddGuest({
  chainId,
  lockerAddress,
}: {
  chainId: string;
  lockerAddress: Address;
}) {
  const [modalState, setModalState] = useState(false);
  const [reset, setReset] = useState({});

  const { mint } = useLocker(chainId, lockerAddress);
  const { uploadObject } = useGreenfield();

  const schema = useMemo(
    () =>
      z.object({
        address: z.string().min(1).startsWith("0x"),
        availableAt: z.date(),
        expiredAt: z.date(),
      }),
    []
  );
  type Schema = z.infer<typeof schema>;

  const initialValues = useMemo(
    () => ({
      address: "",
      availableAt: dayjs().toDate(),
      expiredAt: dayjs().add(1, "day").toDate(),
    }),
    []
  );

  const onSubmit: SubmitHandler<Schema> = useCallback(
    async (data) => {
      const { address, availableAt, expiredAt } = data;

      const blob = new Blob(
        [
          JSON.stringify({
            availableAt: dayjs(availableAt).unix(),
            expiredAt: dayjs(expiredAt).unix(),
          }),
        ],
        { type: "application/json" }
      );

      const objectName = window.crypto.randomUUID();
      const sps = await uploadObject({
        objectName,
        file: new File([blob], `${objectName}.json`, {
          type: "application/json",
        }),
      });
      if (sps?.statusCode !== 200) {
        toast.error("Create metadata failed!");
        return;
      }

      mint({
        to: address as Address,
        uri: objectName,
        availableAt: dayjs(availableAt).unix(),
        expiredAt: dayjs(expiredAt).unix(),
        onSuccess: (hash) => {
          toast.success(() => (
            <div className="max-w-72 truncate">
              <span>Added.&nbsp;</span>
              <Web3Transaction chainId={chainId} hash={hash} />
            </div>
          ));
          setReset(initialValues);
          setModalState(false);
        },
      });
    },
    [chainId, initialValues, mint, uploadObject]
  );

  useEffect(() => {
    setReset(initialValues);
  }, [initialValues]);

  return (
    <>
      <ActionIcon
        onClick={() => setModalState(true)}
        variant="solid"
        rounded="full"
      >
        <PiPlus />
      </ActionIcon>
      <Modal isOpen={modalState} onClose={() => setModalState(false)}>
        <div className="m-auto px-7 pb-8 pt-6">
          <div className="mb-7 flex items-center justify-between">
            <Title as="h3">Add new guest</Title>
            <ActionIcon
              size="sm"
              variant="text"
              onClick={() => setModalState(false)}
            >
              <PiXLight />
            </ActionIcon>
          </div>
          <Form<Schema>
            className="gap-x-5 gap-y-3 [&_label>span]:font-medium"
            validationSchema={schema}
            resetValues={reset}
            onSubmit={onSubmit}
            useFormProps={{
              mode: "onChange",
              defaultValues: initialValues,
            }}
          >
            {({ control, register, formState: { errors } }) => (
              <div className="grid grid-cols-1 gap-y-6">
                <Input
                  className="col-span-1"
                  type="text"
                  label="Address"
                  placeholder="0x..."
                  {...register("address")}
                  error={errors.address?.message}
                />
                <Controller
                  control={control}
                  name="availableAt"
                  render={({ field: { onChange, value } }) => (
                    <DatePicker
                      className="col-span-1"
                      selected={value}
                      onChange={onChange}
                      dateFormat="d MMMM yyyy, h:mm aa"
                      showTimeSelect
                      inputProps={{
                        label: "Available At",
                        error: errors.availableAt?.message,
                      }}
                    />
                  )}
                />
                <Controller
                  control={control}
                  name="expiredAt"
                  render={({ field: { onChange, value } }) => (
                    <DatePicker
                      className="col-span-1"
                      selected={value}
                      onChange={onChange}
                      dateFormat="d MMMM yyyy, h:mm aa"
                      showTimeSelect
                      inputProps={{
                        label: "Expired At",
                        error: errors.availableAt?.message,
                      }}
                    />
                  )}
                />
                <Button type="submit" className="col-span-1 mt-72">
                  Add
                </Button>
              </div>
            )}
          </Form>
        </div>
      </Modal>
    </>
  );
}
