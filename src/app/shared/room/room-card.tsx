"use client";

import Link from "next/link";
import { Address } from "viem";
import { useChainId } from "wagmi";

import WidgetCard from "@/components/cards/widget-card";
import useLocker from "@/hooks/use-locker";
import { truncateEthAddress } from "@/utils/web3";

export default function RoomCard({ address }: { address: Address }) {
  const chainId = useChainId();

  const { name } = useLocker(chainId.toString(), address);

  return (
    <Link href={`/rooms/${chainId}:${address}`}>
      <WidgetCard
        title={name}
        rounded="lg"
        description={truncateEthAddress(address)}
        headerClassName="p-5 lg:p-7"
        className="overflow-hidden p-0 hover:cursor-pointer hover:shadow-md lg:p-0 col-span-1"
      />
    </Link>
  );
}
