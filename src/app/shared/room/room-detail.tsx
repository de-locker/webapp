"use client";

import Link from "next/link";
import { useParams } from "next/navigation";
import { useCallback, useMemo } from "react";
import { Button, Title } from "rizzui";
import { Address, hexToNumber } from "viem";
import { useAccount } from "wagmi";

import BasicTableWidget from "@/components/controlled-table/basic-table-widget";
import { HeaderCell } from "@/components/ui/table";
import useChain from "@/hooks/use-chain";
import useLocker from "@/hooks/use-locker";
import useNodereal from "@/hooks/use-nodereal";
import cn from "@/utils/class-names";
import { useQuery } from "@tanstack/react-query";

import Web3Address from "../web3/web3-address";
import AddGuest from "./add-guest";
import RoomURI from "./room-uri";

export default function RoomDetail() {
  const { roomId } = useParams<{ roomId: string }>();
  const account = useAccount();

  const [chainId, lockerAddress] = useMemo(() => roomId.split("%3A"), [roomId]);
  const chain = useChain(Number(chainId));

  const { name, checkIn } = useLocker(chainId, lockerAddress as Address);
  const { fetcher } = useNodereal(chainId);

  const { data } = useQuery({
    queryKey: [chainId, lockerAddress, "roomOwners"],
    queryFn: async () =>
      (
        await fetcher({
          method: "POST",
          body: JSON.stringify({
            jsonrpc: "2.0",
            method: "nr_getNFTCollectionHolders",
            params: [lockerAddress, "ERC721", "0x14", "", true],
            id: 1,
          }),
        })
      )?.json(),
  });

  const getColumns = useCallback(
    () => [
      {
        title: <HeaderCell title="Address" />,
        dataIndex: "holderAddress",
        key: "holderAddress",
        render: (holderAddress: Address) => (
          <Web3Address chainId={chainId} address={holderAddress} />
        ),
      },
      {
        title: <HeaderCell title="Key ID" align="center" />,
        dataIndex: "tokenId",
        key: "tokenId",
        align: "center",
        render: (tokenId: number) => (
          <Link
            className="text-primary hover:text-primary-dark"
            href={`${chain?.blockExplorers?.default.url}/token/${lockerAddress}/${tokenId}`}
            target="_blank"
            rel="nofollow noopener noreferrer"
          >
            {tokenId}
          </Link>
        ),
      },
      {
        title: <HeaderCell title="Metadata" align="center" />,
        dataIndex: "tokenId",
        key: "tokenId",
        align: "center",
        render: (tokenId: number) => (
          <RoomURI
            chainId={chainId}
            lockerAddress={lockerAddress as Address}
            tokenId={tokenId}
          />
        ),
      },
      {
        dataIndex: "tokenId",
        key: "tokenId",
        align: "right",
        render: (tokenId: number, row: any) => (
          <div>
            {account.address?.toLowerCase() ===
              row.holderAddress.toLowerCase() && (
              <Button onClick={() => checkIn({ tokenId: BigInt(tokenId) })}>
                Check-in
              </Button>
            )}
          </div>
        ),
      },
    ],
    [
      account.address,
      chain?.blockExplorers?.default.url,
      chainId,
      checkIn,
      lockerAddress,
    ]
  );

  return (
    <div className="px-4">
      <div>
        <Web3Address chainId={chainId} address={lockerAddress as Address}>
          <Title>{name}</Title>
        </Web3Address>
      </div>

      <BasicTableWidget
        title="All guests"
        enableSearch={false}
        className={cn(
          "mb-3 pb-0 lg:pb-0 [&_.rc-table-row:last-child_td]:border-b-0 mt-4"
        )}
        data={data?.result?.holderAddressesWithBalances
          .map((holder: any) =>
            holder.tokenBalances.map((item: any) => ({
              id: item.tokenId,
              holderAddress: holder.holderAddress,
              tokenId: hexToNumber(item.tokenId),
            }))
          )
          .flat()
          .sort((a: any, b: any) => b.tokenId - a.tokenId)}
        getColumns={getColumns}
        noGutter
        scroll={{
          x: 900,
        }}
        action={
          <AddGuest
            chainId={chainId}
            lockerAddress={lockerAddress as Address}
          />
        }
      />
    </div>
  );
}
