"use client";

import { useChainId } from "wagmi";

import useLockerFactory from "@/hooks/use-locker-factory";

import RoomCard from "../room/room-card";
import SectionBlock from "../section-block";

export default function ListRoom() {
  const chainId = useChainId();

  const { lockers } = useLockerFactory(chainId.toString());

  return (
    <div className="px-4">
      <SectionBlock title="All Rooms" titleClassName="text-xl sm:text-2xl">
        <div className="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 3xl:gap-8">
          {lockers?.map((locker) => (
            <RoomCard key={locker} address={locker} />
          ))}
        </div>
      </SectionBlock>
    </div>
  );
}
