import { metaObject } from "@/config/site.config";
import ListRoom from "../shared/host/list-room";

export const metadata = {
  ...metaObject("Your Rooms"),
};

export default function HostPage() {
  return (
    <>
      <ListRoom />
    </>
  );
}
