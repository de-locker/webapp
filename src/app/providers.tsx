"use client";

import { ConnectKitProvider } from "connectkit";
import { Provider } from "jotai";
import { ReactNode, useState } from "react";
import { State, WagmiProvider } from "wagmi";

import { ThemeProvider } from "@/components/theme-provider";
import { config } from "@/config/web3";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

export default function Providers({
  children,
  initialWagmiState,
}: {
  children: ReactNode;
  initialWagmiState?: State | undefined;
}) {
  const [queryClient] = useState(() => new QueryClient());

  return (
    <WagmiProvider config={config} initialState={initialWagmiState}>
      <QueryClientProvider client={queryClient}>
        <ConnectKitProvider>
          <ThemeProvider>
            <Provider>{children}</Provider>
          </ThemeProvider>
        </ConnectKitProvider>
      </QueryClientProvider>
    </WagmiProvider>
  );
}
