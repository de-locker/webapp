import { metaObject } from "@/config/site.config";
import Link from "next/link";
import { Button, Title } from "rizzui";

export const metadata = {
  ...metaObject("DeLocker"),
};

export default function Home() {
  return (
    <div className="flex justify-center items-center w-full flex-1">
      <div className="flex flex-col gap-4">
        <Title>Welcome to DeLocker</Title>
        <div className="flex gap-8 justify-center">
          <Link href="/host">
            <Button>Use as Host</Button>
          </Link>
          <Link href="/guest">
            <Button variant="outline">Use as Guest</Button>
          </Link>
        </div>
      </div>
    </div>
  );
}
