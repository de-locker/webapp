'use client';

import { ChainIcon, ConnectKitButton } from 'connectkit';
import { PiWalletDuotone } from 'react-icons/pi';
import { ActionIcon, Button, Dropdown } from 'rizzui';
import { useChainId, useSwitchChain } from 'wagmi';

export default function ConnectWalletButton() {
  const chainId = useChainId();
  const { chains, switchChain } = useSwitchChain();

  return (
    <ConnectKitButton.Custom>
      {({ isConnected, show, truncatedAddress, chain }) => {
        return (
          <div className="flex gap-2">
            <Dropdown placement="bottom-end">
              <Dropdown.Trigger>
                <ActionIcon
                  variant="outline"
                  rounded="full"
                  className="h-auto w-auto"
                >
                  <ChainIcon id={chainId} size={30} />
                </ActionIcon>
              </Dropdown.Trigger>
              <Dropdown.Menu className="divide-y">
                {chains.map((c) => (
                  <Dropdown.Item
                    key={c.id}
                    className="space-x-2"
                    onClick={() => switchChain({ chainId: c.id })}
                  >
                    <ChainIcon id={c.id} />
                    <span className="truncate">{c.name}</span>
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
            <Button
              variant="outline"
              rounded="pill"
              onClick={show}
              className="space-x-2"
            >
              <PiWalletDuotone className="h-5 w-5" />
              <span>{isConnected ? truncatedAddress : 'Connect Wallet'}</span>
            </Button>
          </div>
        );
      }}
    </ConnectKitButton.Custom>
  );
}
