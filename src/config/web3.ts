import { Address, ChainContract } from "viem";
import { cookieStorage, createConfig, createStorage, http } from "wagmi";
import { Chain, opBNBTestnet } from "wagmi/chains";
import { injected } from "wagmi/connectors";

const opBNBTestnetConfig = {
  ...opBNBTestnet,
  contracts: {
    lockerFactory: {
      address: "0x8aBeEEdd881fd2FE31B811230A940B30E0Ad4fC6",
    },
  },
};

const chains: { [key: string]: readonly [Chain, ...Chain[]] } = {
  mainnet: [opBNBTestnetConfig],
  testnet: [opBNBTestnetConfig],
};

export function getContractAddress(chain: Chain, name: string): Address {
  return (
    (chain?.contracts?.[name] as ChainContract | undefined)?.address || "0x"
  );
}

export function hasContractAddress(chain: Chain, name: string): boolean {
  return (
    (chain?.contracts?.[name] as ChainContract | undefined)?.address !==
    undefined
  );
}

export const config = createConfig({
  ssr: true,
  storage: createStorage({
    storage: cookieStorage,
  }),
  // Your dApps chains
  chains: chains[process.env.NEXT_PUBLIC_CHAIN_MODE || "testnet"],
  transports: {
    // RPC URL for each chain
    [opBNBTestnet.id]: http("https://opbnb-testnet-rpc.bnbchain.org"),
  },

  connectors: [injected({ target: "metaMask" })],
});
