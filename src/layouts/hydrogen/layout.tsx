import Header from "./header";

export default function HydrogenLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <>
      <main className="flex flex-col min-h-screen flex-grow">
        <Header />
        <>{children}</>
      </main>
    </>
  );
}
