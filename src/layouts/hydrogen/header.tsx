"use client";

import ConnectWalletButton from "@/components/connect-wallet-button";
import StickyHeader from "@/layouts/sticky-header";
import Link from "next/link";
import { Title } from "rizzui";

export default function Header() {
  return (
    <StickyHeader className="z-[990] 2xl:py-5">
      <div className="flex items-center flex-end w-full justify-between">
        <div>
          <Link href="/">
            <Title as="h1">DeLocker</Title>
          </Link>
        </div>
        <ConnectWalletButton />
      </div>
    </StickyHeader>
  );
}
