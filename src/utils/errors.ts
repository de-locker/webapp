import toast from 'react-hot-toast';

import { WriteContractErrorType } from '@wagmi/core';

export function handleWriteContractError(error: WriteContractErrorType) {
  console.error(error);
  if (error.name === 'ConnectorNotConnectedError') {
    toast.error('Please connect your wallet first!!!');
  } else if (error.name === 'ContractFunctionExecutionError') {
    toast.error(error.shortMessage);
  } else {
    toast.error(error.message);
  }
}
