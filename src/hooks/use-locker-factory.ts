"use client";

import { useCallback, useMemo } from "react";
import { Address, ChainContract } from "viem";
import { useReadContracts, useWriteContract } from "wagmi";

import { config } from "@/config/web3";
import { lockerFactoryAbi } from "@/contracts/locker-factory";
import { handleWriteContractError } from "@/utils/errors";
import { readContract } from "@wagmi/core";

import useChain from "./use-chain";

export default function useLockerFactory(chainId: string) {
  const chain = useChain(Number(chainId));
  const { writeContract } = useWriteContract();

  const contract = useMemo(
    () => ({
      chainId: chain.id,
      address: (chain?.contracts?.lockerFactory as ChainContract)?.address,
      abi: lockerFactoryAbi,
    }),
    [chain?.contracts?.lockerFactory, chain.id]
  );

  const { data } = useReadContracts({
    contracts: [{ ...contract, functionName: "lockers" }],
  });

  const [lockers] = data || [];

  const lockersOf = useCallback(
    (owner: Address) =>
      readContract(config, {
        ...contract,
        functionName: "lockersOf",
        args: [owner],
      }),
    [contract]
  );

  const newLocker = useCallback(
    ({
      name,
      symbol,
      onSuccess,
    }: {
      name: string;
      symbol: string;
      onSuccess: (hash: string) => void;
    }) => {
      writeContract(
        {
          ...contract,
          functionName: "newLocker",
          args: [name, symbol],
        },
        {
          onSuccess,
          onError: handleWriteContractError,
        }
      );
    },
    [contract, writeContract]
  );

  return { lockers: lockers?.result, lockersOf, newLocker };
}
