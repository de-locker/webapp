"use client";

import { useChainId, useChains } from "wagmi";

export default function useChain(chainId?: number) {
  const chains = useChains();
  const _chainId = useChainId();

  return chains.find((c) => c.id === (chainId || _chainId)) || chains[0];
}
