"use client";

import { useCallback, useMemo } from "react";
import toast from "react-hot-toast";
import { Address } from "viem";
import {
  useAccount,
  useReadContracts,
  useSignMessage,
  useWriteContract,
} from "wagmi";

import { config } from "@/config/web3";
import { lockerAbi } from "@/contracts/locker";
import { handleWriteContractError } from "@/utils/errors";
import { readContract } from "@wagmi/core";

import useChain from "./use-chain";

export default function useLocker(chainId: string, lockerAddress: Address) {
  const chain = useChain(Number(chainId));
  const account = useAccount();
  const { writeContract } = useWriteContract();
  const { signMessageAsync } = useSignMessage();

  const contract = useMemo(
    () => ({
      chainId: chain.id,
      address: lockerAddress,
      abi: lockerAbi,
    }),
    [chain.id, lockerAddress]
  );

  const { data } = useReadContracts({
    contracts: [{ ...contract, functionName: "name" }],
  });

  const [name] = data || [];

  const mint = useCallback(
    ({
      to,
      uri,
      availableAt,
      expiredAt,
      onSuccess,
    }: {
      to: Address;
      uri: string;
      availableAt: number;
      expiredAt: number;
      onSuccess: (hash: string) => void;
    }) => {
      writeContract(
        {
          ...contract,
          functionName: "mint",
          args: [to, uri, BigInt(availableAt), BigInt(expiredAt)],
        },
        {
          onSuccess,
          onError: handleWriteContractError,
        }
      );
    },
    [contract, writeContract]
  );

  const isAvailable = useCallback(
    ({ tokenId }: { tokenId: bigint }) =>
      readContract(config, {
        ...contract,
        functionName: "isAvailable",
        args: [tokenId],
      }),
    [contract]
  );

  const tokenURI = useCallback(
    ({ tokenId }: { tokenId: bigint }) =>
      readContract(config, {
        ...contract,
        functionName: "tokenURI",
        args: [tokenId],
      }),
    [contract]
  );

  const checkedIn = useCallback(
    ({
      data,
      signature,
      tokenId,
    }: {
      data: `0x${string}`;
      signature: `0x${string}`;
      tokenId: bigint;
    }) =>
      readContract(config, {
        ...contract,
        functionName: "checkedIn",
        args: [data, signature, tokenId],
      }),
    [contract]
  );

  const checkIn = useCallback(
    async ({ tokenId }: { tokenId: bigint }) => {
      if (!account.address) return false;

      const message = `Check-in ${lockerAddress} - ${tokenId}`;
      const signature = await signMessageAsync({
        account: account.address,
        message,
      });

      // const checked = await checkedIn({
      //   data: hashMessage(message),
      //   signature,
      //   tokenId,
      // });

      toast.success("Checked-in");
      return true;
    },
    [account.address, lockerAddress, signMessageAsync]
  );

  return {
    name: name?.result,
    isAvailable,
    tokenURI,
    mint,
    checkIn,
    checkedIn,
  };
}
