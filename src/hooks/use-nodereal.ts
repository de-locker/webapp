"use client";

import { useCallback, useMemo } from "react";
import { opBNBTestnet } from "viem/chains";

export default function useNodereal(chainId: string) {
  const rpcUrl = useMemo(
    () =>
      ({
        [opBNBTestnet.id]: `https://opbnb-testnet.nodereal.io/v1/${process.env.NEXT_PUBLIC_NODEREAL_API_KEY}`,
      }[Number(chainId)]),
    [chainId]
  );

  const fetcher = useCallback(
    (init?: RequestInit) => (rpcUrl ? fetch(rpcUrl, init) : undefined),
    [rpcUrl]
  );

  return { rpcUrl, fetcher };
}
