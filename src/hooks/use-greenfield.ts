"use client";

import { useCallback, useMemo } from "react";
import { Connector, useAccount } from "wagmi";

import { Client, VisibilityType } from "@bnb-chain/greenfield-js-sdk";

export const signTypedDataV4 = async (
  provider: any,
  addr: string,
  message: string
) => {
  return await provider?.request({
    method: "eth_signTypedData_v4",
    params: [addr, message],
  });
};

export const signTypedDataCallback = (connector: Connector) => {
  return async (addr: string, message: string) => {
    const provider = await connector.getProvider();
    return await signTypedDataV4(provider, addr, message);
  };
};

export default function useGreenfield() {
  const account = useAccount();

  const client = useMemo(
    () =>
      Client.create(
        process.env.NEXT_PUBLIC_GREENFIELD_RPC ||
          "https://gnfd-testnet-fullnode-tendermint-ap.bnbchain.org",
        process.env.NEXT_PUBLIC_GREENFIELD_CHAIN_ID || "5611"
      ),
    []
  );

  const getSps = useCallback(async () => {
    const sps = await client.sp.getStorageProviders();
    const finalSps = (sps ?? []).filter((v: any) =>
      v.endpoint.includes("https")
    );

    return finalSps;
  }, [client.sp]);

  const getAllSps = useCallback(async () => {
    const sps = await getSps();

    return sps.map((sp) => {
      return {
        address: sp.operatorAddress,
        endpoint: sp.endpoint,
        name: sp.description?.moniker,
      };
    });
  }, [getSps]);

  const generateSeed = useCallback(async () => {
    if (!account.address) return;

    const allSps = await getAllSps();
    const provider = await account.connector?.getProvider();
    return await client.offchainauth
      .genOffChainAuthKeyPairAndUpload(
        {
          sps: allSps,
          chainId: Number(
            process.env.NEXT_PUBLIC_GREENFIELD_CHAIN_ID || "5611"
          ),
          expirationMs: 5 * 24 * 60 * 60 * 1000,
          domain: window.location.origin,
          address: account.address,
        },
        provider
      )
      .then((res) => res.body);
  }, [account.address, account.connector, client.offchainauth, getAllSps]);

  const uploadObject = useCallback(
    async ({ objectName, file }: { objectName: string; file: File }) => {
      if (!account.address || !account.connector) return;

      const bucketName =
        process.env.NEXT_PUBLIC_GREENFIELD_BUCKET || "de-locker";

      const offChainData = await generateSeed();
      if (!offChainData) return;

      return await client.object.delegateUploadObject(
        {
          bucketName,
          objectName,
          body: file,
          delegatedOpts: {
            visibility: VisibilityType.VISIBILITY_TYPE_PUBLIC_READ,
          },
        },
        {
          type: "EDDSA",
          domain: window.location.origin,
          seed: offChainData.seedString,
          address: account.address,
        }
      );
    },
    [account.address, account.connector, client.object, generateSeed]
  );

  const getObject = useCallback(
    ({ objectName }: { objectName: string }) => {
      const bucketName =
        process.env.NEXT_PUBLIC_GREENFIELD_BUCKET || "de-locker";
      return client.object.headObject(bucketName, objectName);
    },
    [client.object]
  );

  return { client, uploadObject, getObject };
}
